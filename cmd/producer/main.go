package main

import (
	"fmt"
	"log"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func main() {
	producer, err := kafka.NewProducer(
		&kafka.ConfigMap{
			"bootstrap.servers": "localhost:9092",
		},
	)
	if err != nil {
		log.Fatalf("uknown error - %v", err)
	}
	defer producer.Close()

	// Delivery report handler for produced messages
	go func() {
		for e := range producer.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					log.Printf("delivery failed: %s", ev.TopicPartition)
				} else {
					log.Printf("delivered message to %s", ev.TopicPartition)
				}
			default:
				log.Printf("unknown event type - %T", e)
			}
		}
	}()

	// Produce messages to topic (asynchronously)
	topic := "first_topic"

	for i := 0; ; i++ {
		if err := producer.Produce(
			&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic: &topic, Partition: kafka.PartitionAny,
				},
				Value: []byte(fmt.Sprintf("test_message_%d", i)),
			},
			nil,
		); err != nil {
			log.Fatalf("uknown error - %v", err)
		}

		time.Sleep(time.Second)
	}

	// Wait for message deliveries before shutting down
	producer.Flush(15 * 1000)
}
