package main

import (
	"log"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func main() {
	consumer, err := kafka.NewConsumer(
		&kafka.ConfigMap{
			"bootstrap.servers": "localhost:9092",
			"group.id":          "myGroup",
		},
	)
	if err != nil {
		log.Fatalf("uknown error - %v", err)
	}
	defer func() {
		if err := consumer.Close(); err != nil {
			log.Fatalf("uknown error - %v", err)
		}
	}()

	if err := consumer.SubscribeTopics([]string{"first_topic"}, nil); err != nil {
		log.Fatalf("uknown error - %v", err)
	}

	for {
		msg, err := consumer.ReadMessage(-1)
		if err != nil {
			log.Printf("uknown error - %v", err)
			continue
		}

		log.Printf("message on %s: %s", msg.TopicPartition, msg.Value)
	}
}
